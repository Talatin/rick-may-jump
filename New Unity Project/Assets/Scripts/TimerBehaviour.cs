﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class TimerBehaviour : MonoBehaviour
{
    public static TimerBehaviour instance;
    private float timer;
    public bool isRunning = false;
    [SerializeField] private TMP_Text timeDisplay;
    [SerializeField] private TMP_Text levelNameDisplay;


    // Start is called before the first frame update

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        levelNameDisplay.text = SceneManager.GetActiveScene().name;
    }

    void Update()
    {
        if (isRunning)
        {
            timer += Time.deltaTime;
            string[] tempArr = timer.ToString("0.000").Split(',');
            timeDisplay.text = $"{tempArr[0]}.<size=25>{tempArr[1]}</size>";
        }   
    }

    public void StartTime()
    {
        timer = 0;
        isRunning = true;
    }

    public void StopTime(LeaderBoardNames name)
    {
        isRunning = false;
        //int result = (int)(timer * -1000);
        int result = (int)((timer * -1000) + 1000000);
        timeDisplay.text = timer.ToString("0.000");
        LeaderBoard.instance.SendLeaderboard(result, name);
        //save time 
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;

public class LevelEndHUD : MonoBehaviour
{
    [SerializeField] private GameObject LeaderboardEntry;
    private bool delay = false;
    private void OnEnable()
    {
        if (delay)
        {
            StartCoroutine(DelayLeaderBoard());
        }
        LeaderboardEntry.SetActive(false);
        delay = true;
    }

    private void Update()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void LoadLevelSelect()
    {
        SceneManager.LoadScene("LevelSelect");
    }

    public void GetLeaderboard()
    {

        var request2 = new GetLeaderboardAroundPlayerRequest { PlayFabId = LeaderBoard.instance.playFabId, StatisticName = SceneManager.GetActiveScene().name, MaxResultsCount = 1 };
        PlayFabClientAPI.GetLeaderboardAroundPlayer(request2, OnLeaderboardGetPlay, OnError);
    }

    private void DisplayLeaderboard(string[] data)
    {
        LeaderboardEntry.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = data[0];
        LeaderboardEntry.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = data[1];
        LeaderboardEntry.transform.GetChild(2).GetChild(0).GetComponent<TMP_Text>().text = data[2];
        LeaderboardEntry.SetActive(true);
    }

    private void OnLeaderboardGetPlay(GetLeaderboardAroundPlayerResult result)
    {
        if (result.Leaderboard[0].StatValue == 0)
        {
            return;
        }

        string time = (result.Leaderboard[0].StatValue * -1 + 1000000).ToString();
        time = time.Insert(time.Length - 3, ",");
        string[] data = new string[3];
        data[0] = (result.Leaderboard[0].Position + 1).ToString();
        data[1] = result.Leaderboard[0].DisplayName;
        data[2] = time;
        DisplayLeaderboard(data);
    }
    private void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging in or creating account");
        Debug.Log(error.GenerateErrorReport());
    }

    IEnumerator DelayLeaderBoard()
    {
        GetLeaderboard();
        yield return new WaitForSeconds(0.25f);
        GetLeaderboard();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;

public class LeaderBoardManager : MonoBehaviour
{

    [SerializeField] private int maxLeaderboardEntries;
    [SerializeField] private GameObject leaderboardEntry;
    [SerializeField] private Transform leaderboardParent;
    [SerializeField] private TMP_Dropdown dropdown;



    // Start is called before the first frame update
    void Start()
    {
        GetLeaderboard(0);
    }

    private void Update()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void GetLeaderboard(int id)
    {
        ClearLeaderboard();

        var request = new GetLeaderboardRequest
        {
            StatisticName = dropdown.options[id].text,
            StartPosition = 0,
            MaxResultsCount = maxLeaderboardEntries
        };

        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);
    }

    private void DisplayLeaderboard(string[] data)
    {
        GameObject temp = Instantiate(leaderboardEntry, leaderboardParent);
        temp.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().text = data[0];
        temp.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = data[1];
        temp.transform.GetChild(2).GetChild(0).GetComponent<TMP_Text>().text = data[2];
    }

    private void ClearLeaderboard()
    {
        for (int i = 0; i < leaderboardParent.childCount; i++)
        {
            Destroy(leaderboardParent.GetChild(i).gameObject);
        }
    }

    private void OnLeaderboardGet(GetLeaderboardResult result)
    {
        int scoreOffset = 0;
        foreach (var item in result.Leaderboard)
        {
            if (item.StatValue == 0)
            {
                scoreOffset++;
                continue;
            }
            string time = (item.StatValue * -1 + 1000000).ToString();
            time = time.Insert(time.Length - 3, ",");
            string[] data = new string[3];
            data[0] = (item.Position + 1 - scoreOffset).ToString();
            data[1] = item.DisplayName;
            data[2] = time;
            DisplayLeaderboard(data);
        }
    }
    private void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging in or creating account");
        Debug.Log(error.GenerateErrorReport());
    }
}

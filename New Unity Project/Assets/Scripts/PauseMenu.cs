﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pauseCanvas;
    [SerializeField] private EventSystem sus;
    [SerializeField] private GameObject continueButton;
    public static PauseMenu instance;
    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            TogglePauseMenu();
        }
    }

    public void QuitGame()
    {
        TogglePauseMenu();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

    public void TogglePauseMenu()
    {
        pauseCanvas.SetActive(!pauseCanvas.activeSelf);
        Time.timeScale = pauseCanvas.activeSelf ? 0 : 1;
        Cursor.visible = pauseCanvas.activeSelf;
        Cursor.lockState = pauseCanvas.activeSelf ? CursorLockMode.None : CursorLockMode.Confined;
        if (pauseCanvas.activeSelf)
        {
            sus.SetSelectedGameObject(continueButton);
        }
    }

    public void ReloadScene()
    {
        TogglePauseMenu();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadLevelSelect()
    {
        TogglePauseMenu();
        SceneManager.LoadScene("LevelSelect");
    }


}

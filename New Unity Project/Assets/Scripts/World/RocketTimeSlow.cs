﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketTimeSlow : MonoBehaviour
{
    [SerializeField] private float EffectPower;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    /// <summary>
    /// Slows the velocity of entering rockets by the EffectPower.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Rigidbody2D rb) && collision.CompareTag("Projectile"))
        {
            rb.velocity = rb.velocity / EffectPower;
        }
    }

    /// <summary>
    /// Speeds up the velocity of exeting rockets by the EffectPower.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Rigidbody2D rb) && collision.CompareTag("Projectile"))
        {
            rb.velocity = rb.velocity * EffectPower;
        }
    }


}

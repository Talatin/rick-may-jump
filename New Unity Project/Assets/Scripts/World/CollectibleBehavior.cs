﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleBehavior : MonoBehaviour
{
    [SerializeField] private Color[] colorsToLerp;
    [SerializeField] private float lerpTime;
    [SerializeField] private float rotSpeed;
    [SerializeField] private GameObject ParticleSystemToSpawn;
    private SpriteRenderer spRend;
    private int index = 0;
    private int nextIndex = 1;
    private float currentLerpTime;
    [SerializeField] private AudioClip[] collectSounds;
    private void Awake()
    {
        spRend = GetComponent<SpriteRenderer>();
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
        index = Random.Range(0, colorsToLerp.Length - 1);
        currentLerpTime = Random.Range(0f, lerpTime);
        nextIndex = index + 1;
    }


    private void Update()
    {
        transform.Rotate(transform.forward * rotSpeed * Time.deltaTime);
        currentLerpTime += Time.deltaTime;
        float perc = currentLerpTime / lerpTime;
        spRend.color = Color.Lerp(colorsToLerp[index], colorsToLerp[nextIndex], perc);
        if (currentLerpTime >= lerpTime)
        {
            currentLerpTime = 0;
            index++;
            nextIndex++;
            if (index >= colorsToLerp.Length)
            {
                index = 0;
            }
            if (nextIndex >= colorsToLerp.Length)
            {
                nextIndex = 0;
            }
        }
    }

    private void Ded()
    {
        GameObject temp = Instantiate(ParticleSystemToSpawn, transform.position, Quaternion.identity);
        ParticleSystem.MainModule main = temp.GetComponent<ParticleSystem>().main;
        main.startColor = spRend.color;
        temp.GetComponent<AudioSource>().clip = collectSounds[Random.Range(0, collectSounds.Length)];
        temp.GetComponent<AudioSource>().Play();
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Ded();
            ParticleSystem.MainModule temp = collision.GetComponent<SoldierFiring>().Rocket.GetComponent<RocketBehavior>().Explosion.GetComponent<ParticleSystem>().main;
            temp.startColor = spRend.color;

            ParticleSystem.MainModule temp2 = collision.GetComponent<ParticleSystem>().main;
            temp2.startColor = spRend.color;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    private Collider2D coll;
    [SerializeField] private Animator anim;
    [SerializeField] private Color[] colorsToLerp;
    [SerializeField] private float lerpTime;
    [SerializeField] private GameObject ParticleSystemToSpawn;
    private SpriteRenderer spRend;
    private int index = 0;
    private int nextIndex = 1;
    private float currentLerpTime;
    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collider2D>();
        spRend = GetComponent<SpriteRenderer>();
        index = Random.Range(0, colorsToLerp.Length - 1);
        currentLerpTime = Random.Range(0f, lerpTime);
        nextIndex = index + 1;
    }

    // Update is called once per frame
    void Update()
    {
        currentLerpTime += Time.deltaTime;
        float perc = currentLerpTime / lerpTime;
        spRend.color = Color.Lerp(colorsToLerp[index], colorsToLerp[nextIndex], perc);
        if (currentLerpTime >= lerpTime)
        {
            currentLerpTime = 0;
            index++;
            nextIndex++;
            if (index >= colorsToLerp.Length)
            {
                index = 0;
            }
            if (nextIndex >= colorsToLerp.Length)
            {
                nextIndex = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StartCoroutine(TutorialStuff());
        }
    }


    IEnumerator TutorialStuff()
    {
        GameObject temp = Instantiate(ParticleSystemToSpawn, transform.position, Quaternion.identity);
        ParticleSystem.MainModule main = temp.GetComponent<ParticleSystem>().main;
        main.startColor = spRend.color;
        coll.enabled = false;
        anim.SetTrigger("Start");
        yield return new WaitForSeconds(3f);
        anim.SetTrigger("End");
        yield return new WaitForSeconds(0.75f);
        coll.enabled = true;
    }

}

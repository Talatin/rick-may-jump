﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public Vector3 PlayerResetOffset;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.TryGetComponent(out SoldierMovement movement))
        {
            movement.CheckPointPos = transform.position + PlayerResetOffset;
            GetComponent<ParticleSystem>().Play();
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawCube(transform.position + PlayerResetOffset, new Vector2(1, 2));
    }
}

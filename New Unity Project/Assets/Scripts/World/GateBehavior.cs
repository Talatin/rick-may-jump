﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateBehavior : MonoBehaviour
{
    [SerializeField] private Color activeColor;
    [SerializeField] private Color inActiveColor;
    [SerializeField] private float lerpTime;
    [SerializeField] private float gateTime = 3;
    private float currentGateTime;
    private bool isActive = true;
    private float currentLerpTime;
    private Collider2D coll;
    private SpriteRenderer spRend;
    private AudioSource source;
    [SerializeField] private AudioClip[] OpenAndCloseSound;

    // Start is called before the first frame update
    void Awake()
    {
        source = GetComponent<AudioSource>();
        spRend = GetComponent<SpriteRenderer>();
        coll = GetComponent<Collider2D>();
        currentLerpTime = lerpTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            currentLerpTime += Time.deltaTime;
            float perc = currentLerpTime / lerpTime;
            spRend.color = Color.Lerp(inActiveColor, activeColor, perc);
            if (currentLerpTime >= lerpTime)
            {
                currentLerpTime = lerpTime;
                coll.enabled = true;
            }
        }
        else
        {
            coll.enabled = false;
            currentLerpTime -= Time.deltaTime;
            float perc = currentLerpTime / lerpTime;
            spRend.color = Color.Lerp(inActiveColor, activeColor, perc);
            if (currentLerpTime <= 0)
            {
                currentLerpTime = 0;
            }
            currentGateTime += Time.deltaTime;
            if (currentGateTime >= gateTime)
            {
                source.PlayOneShot(OpenAndCloseSound[1]);
                isActive = true;
            }

        }
    }

    public void ActivateGate()
    {
        source.PlayOneShot(OpenAndCloseSound[0]);
        isActive = false;
        currentGateTime = 0;
        //StartCoroutine(InActivityDelay());
    }

    IEnumerator InActivityDelay()
    {
        yield return new WaitForSeconds(3);
        isActive = true;
        StopAllCoroutines();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetFloor : MonoBehaviour
{
    /// <summary>
    /// Tries to get the PlayerMovement component and calls the reset position method
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.TryGetComponent(out SoldierMovement sMove))
        {
            sMove.ResetPosition();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Activator : MonoBehaviour
{
    public UnityEvent Action;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        if (Action == null)
            Action = new UnityEvent();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Invokes the events according actions if hit by a projectile
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            //TODO: PlayerFeedback!
            anim.SetTrigger("FEEDBACK");
            Action.Invoke();
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelector : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject endHUD;
    [SerializeField] private string LevelName;
    [SerializeField] private int LevelIndex;
    public bool isInstantLoad = true;
    [SerializeField] private AudioClip[] winSounds;
    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        if (!isInstantLoad)
        {
            endHUD = GameObject.Find("LevelEndObj");
            endHUD.SetActive(false); 
        }
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

    /// <summary>
    /// Loads a level when colliding with the player
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (isInstantLoad)
            {
                LoadScene();
            }
            else
            {
                source.PlayOneShot(winSounds[Random.Range(0, winSounds.Length)]);
                endHUD.SetActive(true);
                collision.gameObject.SetActive(false);
                GetComponent<ParticleSystem>().Play();
            }
        }
    }

    public void LoadScene()
    {
        if (LevelName == "")
        {
            SceneManager.LoadScene(LevelIndex);
        }
        else
        {
            SceneManager.LoadScene(LevelName);
        }
    }
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

}

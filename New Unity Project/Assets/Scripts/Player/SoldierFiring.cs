﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SoldierFiring : MonoBehaviour
{

    [Header("Firing")]
    private Transform mouseTarget;
    [SerializeField] private Vector3 fireOffset;
    [SerializeField] private GameObject aimHelpObj;
    [SerializeField] private GameObject rocket;
    [SerializeField] private float firePower;
    [SerializeField] private float fireRate;
    [SerializeField] private Image fireRateImage;
    [SerializeField] private float fireBufferTime;
    private float currentFireRate;
    private bool canFire = true;
    private bool fireBuffer = false;
    private float currentFireBufferTime;
    private PlayerSounds pSounds;
    private LineRenderer lRend;
    [SerializeField] private LayerMask aimLaserLayer;

    public GameObject Rocket { get => rocket; }

    // Start is called before the first frame update
    void Start()
    {
        pSounds = GetComponent<PlayerSounds>();
        ParticleSystem.MainModule temp = rocket.GetComponent<RocketBehavior>().Explosion.GetComponent<ParticleSystem>().main;
        temp.startColor = Color.white;
        mouseTarget = GameObject.Find("MouseHolder").transform;
        lRend = mouseTarget.GetComponent<LineRenderer>();
        fireRateImage.fillAmount = 1;
        currentFireBufferTime = fireBufferTime;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine(transform.position, mouseTarget.position,Color.white);
        RaycastHit2D[] hitInfo = new RaycastHit2D[1];
        if (Physics2D.RaycastNonAlloc(transform.position, mouseTarget.position - transform.position, hitInfo, Vector2.Distance(transform.position,mouseTarget.position), aimLaserLayer) > 0)
        {
            lRend.SetPosition(0, transform.position + (mouseTarget.position - transform.position).normalized);
            lRend.SetPosition(1, hitInfo[0].point);
        }
        else
        {
            lRend.SetPosition(0, transform.position + (mouseTarget.position - transform.position).normalized);
            lRend.SetPosition(1, mouseTarget.position);
        }
        HandleFireRate();
        HandleInputs();
        currentFireBufferTime += Time.deltaTime;
        if (currentFireBufferTime >= fireBufferTime)
        {
            fireBuffer = false;
        }
        else
        {
            fireBuffer = true;
        }

        Vector3 dir = mouseTarget.position - transform.position;
        dir.Normalize();
        aimHelpObj.transform.position = transform.position + fireOffset + dir;

    }

    private void HandleInputs()
    {
        if (Input.GetButton("Fire1"))
        {

            currentFireBufferTime = 0;
            //StartCoroutine(FireBufferTimer());
            Fire();
        }
        if (fireBuffer)
        {
            Fire();
        }
    }

    /// <summary>
    /// Increases float by Time between frames
    /// Checks if it is above the fireRate threshhold and sets the canFire bool accordingly
    /// </summary>
    private void HandleFireRate()
    {
        if (!canFire)
        {
            currentFireRate += Time.deltaTime;
            fireRateImage.fillAmount = currentFireRate / fireRate;
            if (currentFireRate >= fireRate)
            {
                canFire = true;
            }
        }
    }


    /// <summary>
    /// Calculates the direction to the cursor object.
    /// Spawns a rocket and fires it at that direction.
    /// </summary>
    private void Fire()
    {
        if (canFire)
        {
            pSounds.PlaySound(PlayerSoundType.Rocket);
            currentFireRate = 0;
            canFire = false;
            fireBuffer = false;
            //StopCoroutine(FireBufferTimer());
            Vector3 direction = (mouseTarget.position - transform.position).normalized;
            GameObject temp = Instantiate(rocket, transform.position + fireOffset, transform.rotation);
            temp.GetComponent<Rigidbody2D>().AddForce(direction * firePower, ForceMode2D.Impulse);
            temp.transform.up = direction;
        }
    }
    IEnumerator FireBufferTimer()
    {
        yield return new WaitForSeconds(fireBufferTime);
        fireBuffer = false;
    }

}

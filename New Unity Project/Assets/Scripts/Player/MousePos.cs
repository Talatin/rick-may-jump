﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePos : MonoBehaviour
{
    private Vector2 cameraExtent;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
    }

    // Update is called once per frame
    void Update()
    {
        cameraExtent.y = cam.orthographicSize * 2f;
        cameraExtent.x = cameraExtent.y * cam.aspect;
        //sets the objects position at the cursors screen coordinates with a z value of 0
        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pz = new Vector3(Mathf.Clamp(pz.x,cam.transform.position.x + -cameraExtent.x / 2, cam.transform.position.x + cameraExtent.x / 2), Mathf.Clamp(pz.y, cam.transform.position.y + -cameraExtent.y / 2, cam.transform.position.y + cameraExtent.y / 2), 0);
        transform.position = pz;
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public enum PlayerSoundType { Rocket, Shotgun, Jump, Reset, FastFly }

public class PlayerSounds : MonoBehaviour
{
    [SerializeField] private AudioSource windSource;
    [SerializeField] private AudioClip windSound;
    [SerializeField] private AnimationCurve windVolumeCurve;
    [SerializeField] private AudioClip[] shotgunSounds;
    [SerializeField] private AudioClip[] rocketSounds;
    [SerializeField] private AudioClip[] jumpSounds;
    [SerializeField] private AudioClip[] resetSounds;
    [SerializeField] private AudioClip[] fastFlySounds;

    [SerializeField] private AudioSource source;
    private Rigidbody2D rb;
    private int shotgunID = -1;
    private int rocketID = -1;
    private int jumpID = -1;
    private int resetID = -1;
    private int fastFlyID = -1;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        HandleWindSound();
    }

    private void HandleWindSound()
    {
        windSource.volume = windVolumeCurve.Evaluate(rb.velocity.y / 30);
    }

    public void PlaySound(PlayerSoundType type)
    {
        switch (type)
        {
            case PlayerSoundType.Rocket:
                rocketID = getNonRepeatID(rocketID, rocketSounds.Length);
                source.PlayOneShot(rocketSounds[rocketID]);
                break;
            case PlayerSoundType.Shotgun:
                shotgunID = getNonRepeatID(shotgunID, shotgunSounds.Length);
                source.PlayOneShot(shotgunSounds[shotgunID]);
                break;
            case PlayerSoundType.Jump:
                jumpID = getNonRepeatID(jumpID, jumpSounds.Length);
                source.PlayOneShot(jumpSounds[jumpID]);
                break;
            case PlayerSoundType.Reset:
                resetID = getNonRepeatID(resetID, resetSounds.Length);
                source.PlayOneShot(resetSounds[resetID]);
                break;
            case PlayerSoundType.FastFly:
                   // fastFlyID = getNonRepeatID(fastFlyID, fastFlySounds.Length);
                    //source.PlayOneShot(fastFlySounds[fastFlyID]); 
                break;
            default:
                break;
        }
    }

    private int getNonRepeatID(int id, int length)
    {
        int rando;
        do
        {
            rando = Random.Range(0, length);

        } while (id == rando);
        return rando;
    }

}

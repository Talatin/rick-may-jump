﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierShotgun : MonoBehaviour
{
    [Header("Firing")]
    private Transform mouseTarget;
    [SerializeField] private GameObject particleShell;
    [SerializeField] private float firePower;
    [SerializeField] private float fireRate;
    [SerializeField] private float offsetStrength;
    private float currentFireRate;
    private bool canFire = true;

    private LineRenderer lRend;
    [SerializeField] private LayerMask ShotgunHitLayer;
    [SerializeField] private Color StartColor;
    [SerializeField] private Color EndColor;

    private PlayerSounds pSounds;

    // Start is called before the first frame update
    void Start()
    {
        pSounds = GetComponent<PlayerSounds>();
        mouseTarget = GameObject.Find("MouseHolder").transform;
        Vector3 direction = (mouseTarget.position - transform.position).normalized;

        lRend = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleInputs();
        HandleFireRate();
    }

    private void Fire()
    {
        if (canFire)
        {
            for (int i = -2; i < 3; i++)
            {
                Vector3 direction = (mouseTarget.position - transform.position).normalized;
                Vector3 directionOffset = Vector3.Cross(direction, transform.forward);
                direction += (directionOffset * i) * offsetStrength;
                GameObject temp = Instantiate(particleShell, transform.position,transform.rotation);
                temp.transform.up = direction.normalized;
                temp.GetComponent<Rigidbody2D>().AddForce(direction * firePower, ForceMode2D.Impulse);
            }
            currentFireRate = 0;
            canFire = false;
        }
    }


    private void FireRay()
    {
        if (canFire)
        {
            pSounds.PlaySound(PlayerSoundType.Shotgun);
            RaycastHit2D[] hit = new RaycastHit2D[1];
            int lineIndexOffset = 0;
            for (int i = -2; i < 3; i++)
            {
                Vector3 direction = (mouseTarget.position - transform.position).normalized;
                Vector3 directionOffset = Vector3.Cross(direction, transform.forward);
                direction += (directionOffset * i) * offsetStrength;


                if (Physics2D.RaycastNonAlloc(transform.position, direction, hit, 500, ShotgunHitLayer) > 0)
                {
                    lRend.SetPosition(i + 2 + lineIndexOffset, transform.position + direction);
                    lRend.SetPosition(i + 3 + lineIndexOffset, hit[0].point);

                    GameObject temp = Instantiate(particleShell, hit[0].point, Quaternion.identity);

                    temp.transform.up = -(transform.position - (Vector3)hit[0].point);

                    if (hit[0].transform.TryGetComponent(out Activator act))
                    {
                        act.Action.Invoke();
                    }
                }
                else
                {
                    lRend.SetPosition(i + 2 + lineIndexOffset, transform.position + direction);

                    lRend.SetPosition(i + 3 + lineIndexOffset, transform.position + direction * 250);
                }
                lRend.SetPosition(10, transform.position + direction);
                lineIndexOffset++;
            }
            currentFireRate = 0;
            canFire = false;
        }
    }

    private void HandleInputs()
    {
        if (Input.GetButton("Fire2"))
        {
            //Fire();
            FireRay();
        }
    }
    /// <summary>
    /// Increases float by Time between frames
    /// Checks if it is above the fireRate threshhold and sets the canFire bool accordingly
    /// </summary>
    private void HandleFireRate()
    {
        if (!canFire)
        {
            currentFireRate += Time.deltaTime;
            lRend.startColor = Color.Lerp(StartColor, EndColor, currentFireRate / fireRate);
            lRend.endColor = Color.Lerp(StartColor, EndColor, currentFireRate / fireRate);
            if (currentFireRate >= fireRate)
            {
                canFire = true;
            }
        }
    }


}

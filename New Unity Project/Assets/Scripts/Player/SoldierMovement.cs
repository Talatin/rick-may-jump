﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float speed;
    [SerializeField] private float jumpPower;
    [SerializeField] private float fallMultiplier;
    [SerializeField] private float speedShotTimeFrame;
    [Header("Controls")]
    [SerializeField] private float groundControl;
    [SerializeField] private float airControl;
    [SerializeField] private float jumpBufferTime;
    [SerializeField] private float coyoteTime;
    private float defaultGravity;
    private Rigidbody2D rb;
    private float xAxis;
    private float maxWalkingVelocity;
    private Vector3 checkPointPos;
    private bool jumpBuffer;
    public Vector3 CheckPointPos { get => checkPointPos; set => checkPointPos = value; }

    [Header("GroundCheck")]
    [SerializeField] private bool grounded;
    [SerializeField] private Vector2 groundCheckScale;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Transform groundCheckTransform;
    [SerializeField] private bool movingIsGrounded;

    private ParticleSystem pSys;
    private PlayerSounds pSounds;
    private void Awake()
    {
        pSounds = GetComponent<PlayerSounds>();
        pSys = GetComponent<ParticleSystem>();
        rb = GetComponent<Rigidbody2D>();
        defaultGravity = rb.gravityScale;
        maxWalkingVelocity = speed / 50;
        checkPointPos = transform.position;
    }

    private void Update()
    {
        if (rb.velocity.y > 23f)
        {
            pSys.Play();
            pSounds.PlaySound(PlayerSoundType.FastFly);
        }
        else
        {
            pSys.Stop();
        }
        xAxis = Input.GetAxisRaw("Horizontal");
        if (Input.GetButtonDown("Jump") || jumpBuffer)
        {
            jumpBuffer = true;
            Jump();
            StartCoroutine(JumpBufferTimer());
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GravityManip();
        /*grounded = */GroundCheck();
        if (grounded)
        {
            StartCoroutine(GroundCheckDelay());
        }
        else
        {
            StopCoroutine(GroundCheckDelay());
            movingIsGrounded = false;
        }
        Move();
    }

    /// <summary>
    /// Velocity Lerp as base model
    /// Ground has a stronger control value than in air
    /// velocity can be stacked horizontally
    /// </summary>
    private void Move()
    {
        if (grounded && movingIsGrounded)
        {
            if ((rb.velocity.x <= (-maxWalkingVelocity - 0.5f) && xAxis > 0) || (rb.velocity.x >= (maxWalkingVelocity + 0.5f) && xAxis < 0) || xAxis == 0)
            {
                rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), groundControl * Time.fixedDeltaTime);
            }
            else if (Mathf.Abs(rb.velocity.x) < (maxWalkingVelocity + 0.5f))
            {
                rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), groundControl * Time.fixedDeltaTime);
            }
        }
        else
        {
            if ((rb.velocity.x <= (-maxWalkingVelocity - 0.5f) && xAxis > 0) || (rb.velocity.x >= (maxWalkingVelocity + 0.5f) && xAxis < 0))
            {
                rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), airControl * Time.fixedDeltaTime);
            }
            else if (xAxis == 0)
            {
                rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), (airControl / 2) * Time.fixedDeltaTime);
            }
            else if (Mathf.Abs(rb.velocity.x) < (maxWalkingVelocity + 0.5f))
            {
                rb.velocity = Vector2.Lerp(rb.velocity, new Vector2(xAxis * speed * Time.fixedDeltaTime, rb.velocity.y), airControl * Time.fixedDeltaTime);
            }
        }
    }
    /// <summary>
    /// Adds an impulse force towards world up (No y velocity reset beforehand)
    /// </summary>
    private void Jump()
    {
        if (grounded)
        {
            pSounds.PlaySound(PlayerSoundType.Jump);
            jumpBuffer = false;
            grounded = false;
            if (rb.velocity.y < 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }
            rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        }
    }

    /// <summary>
    /// Checks if there is a collider within the specified layer
    /// </summary>
    /// <returns></returns>
    private void GroundCheck()
    {
        if (Physics2D.OverlapBox(groundCheckTransform.position, groundCheckScale, 0, groundLayer) && rb.velocity.y <= 0.005f)
        {
            grounded = true;
        }
        else
        {
            StartCoroutine(CoyoteTimer());
        }
        //return Physics2D.OverlapBox(groundCheckTransform.position, groundCheckScale, 0, groundLayer) && rb.velocity.y <= 0.005f;
    }

    /// <summary>
    /// Changes the rigidbodies gravityscale dependent on current y velocity.
    /// </summary>
    private void GravityManip()
    {
        if (rb.velocity.y < 0)
        {
            rb.gravityScale = fallMultiplier;
        }
        else
        {
            rb.gravityScale = defaultGravity;
        }
    }

    /// <summary>
    /// Reset the players position to the target and dampens the players velocity
    /// </summary>
    /// <param name="target"></param>
    public void ResetPosition()
    {
        transform.position = checkPointPos;
        rb.velocity /= 10;
        pSounds.PlaySound(PlayerSoundType.Reset);
    }

    /// <summary>
    /// Sets the movingIsGrounded variable true shortly after the player is grounded.
    /// </summary>
    /// <returns></returns>
    IEnumerator GroundCheckDelay()
    {
        yield return new WaitForSeconds(speedShotTimeFrame);
        movingIsGrounded = true;
    }

    IEnumerator JumpBufferTimer()
    {
        yield return new WaitForSeconds(jumpBufferTime);
        jumpBuffer = false;
    }

    IEnumerator CoyoteTimer()
    {
        yield return new WaitForSeconds(coyoteTime);
        grounded = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(groundCheckTransform.position, groundCheckScale);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransfer : MonoBehaviour
{
    public float TransferTime;
    private float currentTime;

    private Camera cam;
    private Vector2 cameraExtent;

    private Transform player;
    private Vector3 Target;
    private Vector3 Current;
    private Rigidbody2D[] physicBodies;
    private Vector3[] savedVelocities;
    private bool traveling;
    private float travelCheckOffset = 0.25f;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Target = transform.position;
        cam = Camera.main;
        //cameraExtent.x = (cam.orthographicSize * Screen.width / Screen.height) + 0.2f;
        //cameraExtent.y = cam.orthographicSize + 0.2f;
        //cameraExtent.y = cam.orthographicSize * 2f;
        //cameraExtent.x = cameraExtent.y * cam.aspect;
    }

    // Update is called once per frame
    void Update()
    {

        cameraExtent.y = cam.orthographicSize * 2f;
        cameraExtent.x = cameraExtent.y * cam.aspect;

        //Lerp between the last and target position of the Camera
        //includes smoothing in and out
        if (traveling)
        {
            currentTime += Time.deltaTime;
            float perc = currentTime / TransferTime;
            perc = perc * perc * perc * perc * (perc * (6f * perc - 15f) + 10f);
            transform.position = Vector3.Lerp(Current, Target, perc);
        }
        else
        {
            currentTime = 0;
            Current = transform.position;
        }

        CheckPlayerPos();
    }
    /// <summary>
    /// Checks on what side the player has left the camera frustum
    /// </summary>
    private void CheckPlayerPos()
    {
        if (!traveling)
        {
            if (player.position.x >= transform.position.x + cameraExtent.x / 2 + travelCheckOffset)
            {
                Target = new Vector3(cameraExtent.x, 0, 0) + transform.position;
                StartCoroutine(FreezeGame());

            }
            if (player.position.x <= transform.position.x - cameraExtent.x / 2 - travelCheckOffset)
            {
                Target = new Vector3(-cameraExtent.x, 0, 0) + transform.position;
                StartCoroutine(FreezeGame());

            }
            if (player.position.y >= transform.position.y + cameraExtent.y / 2 + travelCheckOffset)
            {
                Target = new Vector3(0, cameraExtent.y, 0) + transform.position;
                Rigidbody2D rb = player.GetComponent<Rigidbody2D>();
                if (rb.velocity.y < 13.5f)
                {
                    //rb.AddForce(Vector2.up * 4.5f, ForceMode2D.Impulse);
                    rb.velocity = new Vector2(rb.velocity.x, 13.5f);
                }
                StartCoroutine(FreezeGame());

            }
            if (player.position.y <= transform.position.y - cameraExtent.y / 2 - travelCheckOffset)
            {
                Target = new Vector3(0, -cameraExtent.y, 0) + transform.position;
                StartCoroutine(FreezeGame());
            }
        }
    }
    /// <summary>
    /// Freeze all Rigidbodies and temporarily save the respective velocities
    /// waits the same amount of time the Camera takes to transfer and reenables all physic bodies with their according velocities.
    /// </summary>
    /// <returns></returns>
    IEnumerator FreezeGame()
    {
        traveling = true;
        player.GetComponent<SoldierMovement>().enabled = false;
        player.GetComponent<SoldierFiring>().enabled = false;

        physicBodies = Object.FindObjectsOfType<Rigidbody2D>();
        savedVelocities = new Vector3[physicBodies.Length];
        for (int i = 0; i < physicBodies.Length; i++)
        {

            savedVelocities[i] = physicBodies[i].velocity;
            physicBodies[i].bodyType = RigidbodyType2D.Static;

        }

        yield return new WaitForSeconds(TransferTime);

        for (int i = 0; i < physicBodies.Length; i++)
        {
            if (physicBodies[i])
            {
                physicBodies[i].bodyType = RigidbodyType2D.Dynamic;
                physicBodies[i].velocity = savedVelocities[i];
            }
        }
        player.GetComponent<SoldierMovement>().enabled = true;
        player.GetComponent<SoldierFiring>().enabled = true;
        traveling = false;
    }

}

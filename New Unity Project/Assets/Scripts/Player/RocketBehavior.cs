﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketBehavior : MonoBehaviour
{
    [Range(1,5)]
    [SerializeField] private float PogoHelpMultiplier;
    [SerializeField] private float explodePower;
    [SerializeField] private float explodeRadius;
    [SerializeField] private LayerMask explodeLayer;
    [SerializeField] private GameObject explosion;
    [SerializeField] private GameObject FailedExplosion;
    [SerializeField] private AnimationCurve fallOffCurve;
    private bool canExplode = true;
    public GameObject Explosion { get => explosion; }


    /// <summary>
    /// Gets all rigidbodies in the given explodeRadius
    /// Calculates the direction and applied power to them.
    /// boom.
    /// </summary>
    private void Explode()
    {
        //TODO: reduce fallspeed of player for pogo

        Collider2D[] theyGunnaFly = Physics2D.OverlapCircleAll(transform.position, explodeRadius, explodeLayer);
        foreach (var item in theyGunnaFly)
        {
            if (item.TryGetComponent(out Rigidbody2D flyRB) && item.gameObject != gameObject)
            {
                if (flyRB.bodyType != RigidbodyType2D.Static)
                {
                    Vector2 dir = (item.transform.position - transform.position).normalized * 0.05f;
                    Vector2 colliderPoint = item.GetComponent<Collider2D>().ClosestPoint(transform.position) + dir;

                    dir.Normalize();

                    float perc = ((Vector3)colliderPoint - transform.position).magnitude / explodeRadius;
                    float falloff = fallOffCurve.Evaluate(Mathf.Lerp(0, 1, perc));
                    if (flyRB.velocity.y < 0.1f && transform.position.y < colliderPoint.y)
                    {
                        flyRB.velocity = new Vector2(flyRB.velocity.x, flyRB.velocity.y / PogoHelpMultiplier);
                    }
                    flyRB.AddForce(dir * explodePower * falloff, ForceMode2D.Impulse); 
                }
            }
        }
        Instantiate(explosion, transform.position + (transform.up * -0.5f), Quaternion.identity);
        DestroySelf();
    }

    /// <summary>
    /// Decides wether to explode or fail depending on contact
    /// Instatiates the according particle system
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall"))
        {
            if (canExplode)
            {
                canExplode = false;
                Explode();

            }
        }
        if (collision.CompareTag("NonExWall"))
        {
            Instantiate(FailedExplosion, transform.position, Quaternion.identity);
            DestroySelf();
        }
    }

    private void DestroySelf()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Rigidbody2D>().Sleep();
        GetComponent<Collider2D>().enabled = false;
        Destroy(gameObject,1f);
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explodeRadius);
    }
}

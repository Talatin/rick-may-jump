﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellBehavior : MonoBehaviour
{
    [SerializeField] private GameObject Explosion;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall") || collision.CompareTag("NonExWall"))
        {
            Instantiate(Explosion, transform.position + (transform.up * -0.5f), transform.rotation);
            Destroy(gameObject); 
        }
    }

}

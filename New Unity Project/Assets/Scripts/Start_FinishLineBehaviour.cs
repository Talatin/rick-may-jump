﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Start_FinishLineBehaviour : MonoBehaviour
{
    private enum LineType { Start, Finish }
    [SerializeField] private LineType type;
    [SerializeField] private LeaderBoardNames LevelName;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            switch (type)
            {
                case LineType.Start:
                    if (!TimerBehaviour.instance.isRunning)
                    {
                        TimerBehaviour.instance.StartTime();
                    }
                    break;
                case LineType.Finish:
                    TimerBehaviour.instance.StopTime(LevelName);
                    break;
                default:
                    break;
            } 
        }

    }
}

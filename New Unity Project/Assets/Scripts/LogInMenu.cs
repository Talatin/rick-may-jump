﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LogInMenu : MonoBehaviour
{
    [SerializeField] private TMP_Text usernameText;
    [SerializeField] private Button applyButton;

    private void Start()
    {
    }

    public void UpdateUserName()
    {
        LeaderBoard.instance.UpdatePlayerName(usernameText.text);
    }

    public void CheckForValidInput(string value)
    {
        if (value.Length > 2 && value.Length < 25)
        {
            applyButton.interactable = true;
        }
        else
        {
            applyButton.interactable = false;
        }
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.SceneManagement;

public enum LeaderBoardNames { SpeedRunLevel, Level01, Level02, Level03, Level04, Level05, Level06 }

public class LeaderBoard : MonoBehaviour
{
    public static LeaderBoard instance;
    public string UserName;
    public string playFabId;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

    }


    // Start is called before the first frame update
    void Start()
    {
        Login();
    }

    public void Login()
    {

        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams { GetPlayerProfile = true }
        };
        
        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
    }

    public void UpdatePlayerName(string name)
    {
        var request = new UpdateUserTitleDisplayNameRequest { DisplayName = name };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);
    }

    private void OnSuccess(LoginResult result)
    {
        Debug.Log("Successful login and account creation");
        string name = null;
        if (result.InfoResultPayload.PlayerProfile.DisplayName != null)
        {
            UserName = result.InfoResultPayload.PlayerProfile.DisplayName;
            if (SceneManager.GetActiveScene().buildIndex == 0)
            {
               GameObject.Find("InputField (TMP)").GetComponent<TMPro.TMP_InputField>().text = UserName;
            }
            //Hat n namen go next fam
        }
        else
        {
            //hat keinen namen
        }
    }

    private void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Updated display name");
    }

    private void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging in or creating account");
        Debug.Log(error.GenerateErrorReport());
    }

    public void GetLeaderboard(string name,int amountOfPlaces)
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = name,
            StartPosition = 0,
            MaxResultsCount = amountOfPlaces
        };

        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);
    }

    private void OnLeaderboardGet(GetLeaderboardResult result)
    {
        foreach (var item in result.Leaderboard)
        {
        }
    }

    public void SendLeaderboard(int score, LeaderBoardNames statName)
    {
        var request = new UpdatePlayerStatisticsRequest
        {

            Statistics = new List<StatisticUpdate> {
                new StatisticUpdate {
                    StatisticName = statName.ToString(), Value = score} }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnError);
    }

    private void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Successfull leaderboard sent");
    }
}

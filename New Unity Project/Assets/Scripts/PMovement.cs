﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PMovement : MonoBehaviour
{

    [SerializeField] private float jumpPower;
    [SerializeField] private float speed;

    private Transform mouse;
    private Rigidbody2D rb;
    private float xAxis;
    private bool isGrounded;
    private bool movementIsGrounded = true;

    [SerializeField] private Transform groundCheckPos;
    [SerializeField] private LayerMask checkLayer;
    private Vector2 checkSize;

    // Start is called before the first frame update
    void Start()
    {
        mouse = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        checkSize = new Vector2(transform.localScale.x - 0.05f, transform.localScale.y - 0.05f);
        groundCheckPos.position = transform.position + new Vector3(0, -0.1f);
        isGrounded = GroundCheck();
        Jump();

    }
    private void Jump()
    {
        if (isGrounded && CheckMouseDistance() != Vector2.zero)
        {
            rb.velocity = new Vector2(0, 0);
            rb.AddForce(CheckMouseDistance() * jumpPower, ForceMode2D.Impulse);
        }
        else
        {
            rb.AddForce(new Vector2(CheckMouseDistance().x * jumpPower, 0));

        }
    }

    private Vector2 CheckMouseDistance()
    {
        float dist = Vector2.Distance(transform.position, mouse.position);

        if (Input.GetButton("Fire1") && dist > 2)
        {

            Vector3 temp = (mouse.position - transform.position).normalized;
            if (mouse.position.y > transform.position.y + 1)
            {
                temp.y = 1.3f;
            }
            else
            {
                temp.y = 0;
            }
            return temp.normalized;
        }
        return Vector2.zero;


    }

    private bool GroundCheck()
    {
        Collider2D[] colls = Physics2D.OverlapBoxAll(groundCheckPos.position, checkSize, 0, checkLayer);

        if (colls.Length > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator GroundCheckDelay()
    {
        yield return new WaitForSeconds(1f);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(groundCheckPos.position, checkSize);
    }
}